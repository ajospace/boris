<?php

include './_api3.php';

if (isset ($_POST['appName']))
    {
    if ($_POST['appID'] > 0)
        {
        $ajoApp = R::load ('app', $_POST['appID']);
        }
    else
        {
        $ajoApp = R::dispense ('app');
        }
    if ( ! isset ($_POST['delApp']))
        {
        $ajoApp->appname = $_POST['appName'];
        $ajoApp->appcode = $_POST['htmlText'] . '-||||-' . $_POST['jsText'];
        $i = R::store ($ajoApp);
        }
    elseif( isset ($_POST['delApp']) )
        {
        R::trash($ajoApp);
        header("Location: ./");
        }
    
        
        if(isset($_POST['runApp']))
        {
        $_GET['run'] = 'true';
        }
        
    }


$apps = R::findAll ('app');

$htmlText = "";
$jsText = "";
$appName = "New App Name";
$appID = isset ($_GET['appID']) ? $_GET['appID'] : 0;
if ($appID != 0)
    {
    $app = R::load ('app', $appID);
    $appCode = explode ('-||||-', $app->appcode);
    $htmlText = $appCode[0];
    $jsText = $appCode[1];
    $appName = $app->appname;
    }
?><!DOCTYPE html>
<html>
    <head>
        <title>System</title>

        <link href="_style/style.css" type="text/css"rel="stylesheet"/>
        <link href="_style/animate.css" type="text/css"rel="stylesheet"/>
        <!--
                <link rel="stylesheet" href="_js/sceditor/minified/themes/default.min.css" type="text/css" media="all" />        
        -->        
        <script src="_js/ractive/ractive.min.js"></script>
        <script src="_js/ractive/ractive-transitions-fade.js"></script>
        <script src="_js/jquery/jquery.min.js"></script>

<?php if ( ! isset ($_GET['run']))
    { ?>        
            <script src="_js/codemirror/codemirror.js"></script>
            <script src="_js/codemirror/xml.js"></script>
            <script src="_js/codemirror/css.js"></script>
            <script src="_js/codemirror/htmlmixed.js"></script>
            <script src="_js/codemirror/javascript.js"></script>
            <link href="_js/codemirror/codemirror.css" type="text/css"rel="stylesheet"/>
            <link href="_js/codemirror/xq-light.css" type="text/css"rel="stylesheet"/>
        <?php } ?>        
        <!--            
                <script src="_js/sceditor/minified/jquery.sceditor.bbcode.min.js"></script>
        -->        
        <script src="_js/custom.js"></script>        



    </head>
    <body>
        
        <div id="header">
           
        <nav>
            <ul>
                <li><a href="./">Playground</a></li>
                <li><a href="./download.php">Download</a></li>
                <li><a href="./about.php">About</a></li>
            </ul>
        </nav>
  