     <?php include './_pageheader.php'; ?>      
   <div class="loader" id="mainLoader">  </div>          
            
            <form method="GET" style="position: absolute;left:15px; top:15px; text-align: left; width: 300px">

                <select style="display: inline-block; width: auto" name="appID" id="appID">
                    <option value="0">New</option>
                    <?php foreach ($apps as $Capp)
                        { ?>
                        <option <?= $Capp->id == $appID ? " selected='selected' " : ' ' ?> value="<?= $Capp->id ?>"><?= $Capp->appname ?></option>
                    <?php } ?>
                </select>
                <input type="checkbox" name="run" <?=  isset($_GET['run'])? ' checked="on" ' : '' ?>/>
                <input type="submit" value="Go" >
            </form>
            
            <h1 style="text-align: right; position: absolute;right: 15px; top:0px; margin-top: 3px">
                DaTurA
            </h1>
            <small  style="text-align: right; position: absolute;right: 15px; top:45px">Jquery + Ractivejs + Redbean = Solution<sup>Creativity</sup> = Power </small>

        </div>
        
        <?php if ( ! isset ($_GET['run']))
            { ?>           
            <div id="playground">
                <form method="POST">
                    <input type="hidden" name="appID" value="<?= $appID ?>" />
                    <div id="control">
                        <button class="button-danger" type="submit" id="delApp"  name="delApp">Delete</button>

                        <button class="button-info" type="submit" id="saveApp">Save & Preview</button>
                        <input class="button-success" type="submit" name="runApp" id="runApp" value="Run" />
                    </div>
                    <div id="appname">
                        <h3>App Name</h3>
                        <input type="text" id="appName"  name="appName" value="<?= $appName ?>" />
                    </div>
                    <div id="notes">
                        <h3>Notes</h3>
                        <textarea>Here are some comments</textarea>
                    </div>

                    <div id="html">
                        <h3>HTML</h3>
                        <textarea class="coder" id="htmlText" name="htmlText"><?= $htmlText ?></textarea>
                    </div>                
                    <div id="javascript" >
                        <h3>JS</h3>
                        <textarea id="jsText" name="jsText"><?= $jsText ?></textarea>
                    </div>   
                </form>
            </div>
<?php } ?>
        <div id="display" class="<?= isset($_GET['run'])? ' run ': ''?>">

            <div id="container"></div>
            <script id='template' type='text/ractive'>
<?= $htmlText ?>
            </script>
        </div>

    <script>
        $(function () {

            var ractive = new Ractive({
                el: '#container',
                template: '#template'
            });
            
            $('#delApp').on('click', function(){
               if(confirm("Are you sure you want to delete this app?"))
               {
                   return true;
               }
               else
               {
                   return false;
               }
            });

$('.loader').appendTo('body');
$.ajax({
  // your ajax code
  start: function(){
       $('.loader').fadeIn();
   },
  complete: function(){
       $('.loader').fadeOut();
  }
});
var a = (($(window).width() /2) - ($('#mainLoader').width() /2));
var b = ($(window).height() /2 - ($('#mainLoader').height() /2));
$('#mainLoader').css({
    left: a,
    top: b
});

<?= $jsText ?>

        });
    </script>

<?php include './_pagefooter.php'; ?>        