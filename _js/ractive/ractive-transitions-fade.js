var DEFAULTS = {
	delay: 400,
	duration: 400,
	easing: 'linear'
};

var fadeRactiveOld= function ( t, params ) {
	var targetOpacity;

	params = t.processParams( params, DEFAULTS );

	if ( t.isIntro ) {
		targetOpacity = t.getStyle( 'opacity' );
		t.setStyle( 'opacity', 0 );
                $(t.node).addClass('animated').addClass('zoomIn');
	} else {
		targetOpacity = 0;
                
                $(t.node).addClass('animated').removeClass('zoomIn').addClass('zoomOut');;
	}

	t.animateStyle( 'opacity', targetOpacity, params ).then( t.complete );
};


var fadeRactive= function ( t, params ) {


	params = t.processParams( params, DEFAULTS );

	if ( t.isIntro ) {
                $(t.node).addClass('animated').addClass('fadeIn');
	} else {
                $(t.node).addClass('animated').removeClass('fadeIn').addClass('fadeOut');
	}
        setTimeout(t.complete, 1000);

	//t.animateStyle( 'opacity', targetOpacity, params ).then( t.complete );
};