$(function () {
    Ractive.transitions.fade = fadeRactive;
    var textAreaHeight = $(window).height() - 400;
    $('#playground form div textarea').css({'height': textAreaHeight}).eq(0).hide();



   try  {
        var myCodeMirror = CodeMirror.fromTextArea($('#htmlText')[0]);

        var myCodeMirror2 = CodeMirror.fromTextArea($('#jsText')[0], {mode: 'javascript'});

        myCodeMirror.on('change', function (cMirror) {
            // get value right from instance
            $('#htmlText')[0].value = cMirror.getValue();
        });

        myCodeMirror2.on('change', function (cMirror) {
            // get value right from instance
            $('#jsText')[0].value = cMirror.getValue();
        });
    }
    catch (err){
        //pass
    }
});