<?php

include "_rb/rb.php";
R::setup ('sqlite:dbfile.db');

//some function 
function to_slug($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
}


if(isset($_POST['type']) && !isset($_POST['action'])  )
{
    $type = $_POST['type'];
    parse_str( $_POST['data'], $data );
    $bean = R::dispense($type);
    $bean->import($data);
    $id = R::store($bean);
    $newBean = R::load($type, $id);
    die(json_encode($newBean->export()));
    
}


if(isset($_POST['action']) && $_POST['action'] == 'delete' )
{
    $type = $_POST['type'];
    $bean = R::load($type, $_POST['item']);
    R::trash($bean);
	die('ok');
    
}


if(isset($_GET['type']))
{
    $type = $_GET['type'];
    $beans = R::findAll($type);
    die(json_encode(R::exportAll($beans)));
}
